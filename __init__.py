from classifier.image_classifier import ImageClassifier
from detectors.detector import ContourDetector
import argparse
import logging
import traceback
import cv2 as cv


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--image', help='Input screenshot')
    parser.add_argument('--model', default='model/redraw-cnn.h5', help='Path to the classifier model')
    args = parser.parse_args()
    model_path = args.model
    image_file_path = args.image

    logger.info('Loading model at path: {0}'.format(args.model))

    detector = ContourDetector(image_file_path)
    img_paths = detector.get_cliped_images()
    img_coordinates = detector.get_coordinates()

    temp_replace_string = '../output/'

    try:
        classifier = ImageClassifier(model_path)
        pred_classes = {}
        class_list = []
        for img in img_paths:
            class_name = classifier.get_image_class(img)
            pred_classes[img.replace(temp_replace_string, '')] = class_name
            class_list.append(class_name)
        annoted_img = detector.get_annotated_img(class_list)
        cv.imshow('Annoted Image', annoted_img)
        cv.waitKey(0)
        cv.destroyAllWindows()
        

    except Exception as e:
        traceback.print_exc()
        logger.error('Unable to load model')


