# Image classifier trained on redraw dataset to classify the UI components
# Developed By: Diamond Mohanty
# Date: 01-Aug-2021

# Loading the required packages
from keras.models import load_model
import numpy as np
from tensorflow.keras.preprocessing import image


class ImageClassifier():

  def __init__(self, model_path):
    '''Loads a pre-trained h5 model at the provided path'''
    self.model = load_model(model_path)

  def get_image_class(self, img_path):
    '''Given the image file path returns the the type of android UI component'''
    img = image.load_img(img_path, target_size=(64,64))
    X = image.img_to_array(img)
    X = np.expand_dims(X, axis=0)
    #images = np.vstack([X])
    predicted_val = self.model.predict(X)
    predicted = np.argmax(predicted_val)

    classes = ImageClassifier.get_available_classes()
    for key in classes:
      if classes[key] == predicted:
        return key

  @staticmethod
  def get_available_classes():
    ''' All the classes the ImageClassifier is trained for'''
    return {
        'Button': 0, 
        'CheckBox': 1, 
        'CheckedTextView': 2, 
        'EditText': 3, 
        'ImageButton': 4, 
        'ImageView': 5, 
        'NumberPicker': 6, 
        'ProgressBar': 7, 
        'RadioButton': 8, 
        'RatingBar': 9, 
        'SeekBar': 10, 
        'Spinner': 11, 
        'Switch': 12, 
        'TextView': 13, 
        'ToggleButton': 14
      }


